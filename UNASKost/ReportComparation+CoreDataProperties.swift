//
//  ReportComparation+CoreDataProperties.swift
//  UNASKost
//
//  Created by MacBook on 24/06/21.
//
//

import Foundation
import CoreData


extension ReportComparation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ReportComparation> {
        return NSFetchRequest<ReportComparation>(entityName: "ReportComparation")
    }

    @NSManaged public var searchCategory: String?
    @NSManaged public var searchMethod: String?
    @NSManaged public var searchTime: Double
    @NSManaged public var totalData: Int16

}
