//
//  Color.swift
//  UNASKost
//
//  Created by MacBook on 16/06/21.
//

import UIKit

class Color {
    
    static let shared = Color()
    private init() {}
    
    var dark: UIColor{
        return self.rgbColor(r: 3, g: 2, b: 20, a: 1)
    }
    
    var greenPrimary: UIColor{
        return self.rgbColor(r: 22, g: 180, b: 58, a: 1)
    }
    
    var greenSecondary: UIColor{
        return self.rgbColor(r: 194, g: 235, b: 194, a: 1)
    }
    
    var gray: UIColor{
        return self.rgbColor(r: 245, g: 245, b: 245, a: 1)
    }
    
    var yellow: UIColor{
        return self.rgbColor(r: 233, g: 241, b: 21, a: 1)
    }
    
    func rgbColor(r: CGFloat, g: CGFloat, b: CGFloat, a:CGFloat) -> UIColor {
        return UIColor(red: r / 255, green: g / 255, blue: b / 255, alpha: a)
    }
    
    static func from(hexString: String) -> UIColor? {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
}
