//
//  SearchResultDetailImageSliderCell.swift
//  UNASKost
//
//  Created by MacBook on 20/06/21.
//

import UIKit
import FSPagerView

class SearchResultDetailImageSliderCell: FSPagerViewCell {

    var imageUrl: String?{
        didSet{
            setImage()
        }
    }
    
    @IBOutlet weak var contentImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentImageView.contentMode = .scaleAspectFill
    }

    private func setImage(){
        guard let image = self.imageUrl, let url = URL(string: image) else {
            return
        }
        
        contentImageView.loadImage(url: url)
    }
}
