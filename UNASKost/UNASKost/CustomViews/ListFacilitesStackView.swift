//
//  ListFacilitesStackView.swift
//  UNASKost
//
//  Created by MacBook on 19/06/21.
//

import UIKit


class ListFacilitiesStackView: UIStackView {
    
    var content = [BoardingHouseFacility](){
        didSet{
            self.setupViews()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        self.setupViews()
    }
    
    private func setupViews(){
        
        axis = .horizontal
        distribution = .fillEqually
        
        let dataCount = content.count
        
        guard dataCount > 0 else {return}
        
        let leftFacilityStackView = createFacilityStackView(data: content[0])
        
        addArrangedSubview(leftFacilityStackView)
        
        if dataCount > 1 {
            let rightFacilityStackView = createFacilityStackView(data: content[1])
            addArrangedSubview(rightFacilityStackView)
        }
        
    }
    
    private func createFacilityStackView(data: BoardingHouseFacility) -> UIStackView {
        
        let facilityStackVIew = UIStackView()
        facilityStackVIew.axis = .horizontal
        facilityStackVIew.distribution = .fill
        facilityStackVIew.spacing = 4
        
        let imageContainerView = UIView()
        imageContainerView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        
        let facilityImageView = UIImageView()
        if let url = URL(string: data.imageUrl) {
            facilityImageView.loadImage(url: url)
        }
        
        let facilityNameLabel = UILabel()
        facilityNameLabel.font = .systemFont(ofSize: 12, weight: .light)
        facilityNameLabel.text = data.name
        
        imageContainerView.addSubview(facilityImageView)
        
        facilityImageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate(
            [
                NSLayoutConstraint.init(item: facilityImageView, attribute: .centerX, relatedBy: .equal, toItem: imageContainerView, attribute: .centerX, multiplier: 1, constant: 0),
                NSLayoutConstraint.init(item: facilityImageView, attribute: .centerY, relatedBy: .equal, toItem: imageContainerView, attribute: .centerY, multiplier: 1, constant: 0),
                NSLayoutConstraint.init(item: facilityImageView, attribute: .width, relatedBy: .equal, toItem: .none, attribute: .notAnAttribute, multiplier: 1, constant: 24),
                NSLayoutConstraint.init(item: facilityImageView, attribute: .height, relatedBy: .equal, toItem: .none, attribute: .notAnAttribute, multiplier: 1, constant: 24)
                
            ]
        )
        
        
        facilityStackVIew.addArrangedSubview(imageContainerView)
        facilityStackVIew.addArrangedSubview(facilityNameLabel)
        
        return facilityStackVIew
        
    }
}

