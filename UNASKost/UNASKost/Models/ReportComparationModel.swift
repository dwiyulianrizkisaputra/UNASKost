//
//  ReportComparationModel.swift
//  UNASKost
//
//  Created by MacBook on 24/06/21.
//

import Foundation


struct CreateReportComparation {
    var category: SearchCategoryModel
    var method: String
    var time: Double
    var totalData: Int16
    
    init(category: SearchCategoryModel, method: String, time: Double, totalData: Int16) {
        self.category = category
        self.method = method
        self.time = time
        self.totalData = totalData
    }
}

struct ResultReportComparation {
    //var category: SearchCategoryModel
    var totalData: Int16
    var bubbleSortTime: Double
    var selectionSortTime: Double
    
    init(//category: SearchCategoryModel,
         totalData: Int16, bubbleSortTime: Double, selectionSortTime: Double) {
        //self.category = category
        self.totalData = totalData
        self.bubbleSortTime = bubbleSortTime
        self.selectionSortTime = selectionSortTime
    }
}
