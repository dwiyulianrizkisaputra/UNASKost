//
//  SearchParamModel.swift
//  UNASKost
//
//  Created by MacBook on 18/06/21.
//

import Foundation

struct SearchParamModel{
    var category: SearchCategoryModel
    var type: SearchTypeModel
    var keepPet: SearchKeepPetModel
    var searchMethod: SearchMethodModel
    
    
    init(category: SearchCategoryModel, type: SearchTypeModel, keepPet: SearchKeepPetModel, searchMethod: SearchMethodModel) {
        self.category = category
        self.type = type
        self.keepPet = keepPet
        self.searchMethod = searchMethod
    }
}
