//
//  SearchResultModel.swift
//  UNASKost
//
//  Created by MacBook on 18/06/21.
//

import Foundation

struct SearchResultBoardingHouse{
    var id: String
    var type: String
    var rank: Int
    var placeName: String
    var distance: String
    var areaSize: String
    var price: String
    var longitude: Double
    var latitude: Double
    var keepPetAllowed: Bool
    var image: [BoardingHouseImage]
    var facility: [BoardingHouseFacility]
    
    init(id: String, type: String, rank: Int, placeName: String, distance: String, areaSize: String, price: String, longitude: Double, latitude: Double, keepPetAllowed: Bool, image: [BoardingHouseImage], facility: [BoardingHouseFacility]) {
        self.id = id
        self.type = type
        self.rank = rank
        self.placeName = placeName
        self.distance = distance
        self.areaSize = areaSize
        self.price = price
        self.longitude = longitude
        self.latitude = latitude
        self.keepPetAllowed = keepPetAllowed
        self.image = image
        self.facility = facility
    }
    
}

struct BoardingHouseImage {
    var id: String
    var url: String
    var isMain: Bool
}

struct BoardingHouseFacility {
    var id: String
    var name: String
    var imageUrl: String
}
