//
//  SearchCategoryModel.swift
//  UNASKost
//
//  Created by MacBook on 18/06/21.
//

import Foundation
import UIKit

enum SearchCategoryModel{
    case byPrice
    case byDistance
    case byTotalFacility
    case byTotalArea
    
    var text: String{
        switch self {
        case .byPrice:
            return "price"
        case .byDistance:
            return "distance"
        case .byTotalFacility:
            return "facility"
        case .byTotalArea:
            return "totalArea"
        }
    }
    
    var title: String{
        switch self {
        case .byPrice:
            return "Kategori Harga"
        case .byDistance:
            return "Kategori Jarak Ke Kampus"
        case .byTotalFacility:
            return "Kategori Jumlah Fasilitas"
        case .byTotalArea:
            return "Kategori Luas Tempat"
        }
    }
    
    var image: UIImage{
        switch self {
        case .byPrice:
            return #imageLiteral(resourceName: "icPrice")
        case .byDistance:
            return #imageLiteral(resourceName: "icRoute")
        case .byTotalFacility:
            return #imageLiteral(resourceName: "icFacility")
        case .byTotalArea:
            return #imageLiteral(resourceName: "icSize")
        }
    }
}

enum SearchTypeModel{
    case all
    case mixed
    case boy
    case girl
}

enum SearchKeepPetModel{
    case all
    case allow
    case deny
}

enum SearchMethodModel{
    case bubbleSort
    case selectionSort
}
