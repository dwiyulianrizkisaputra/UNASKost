//
//  ReportTableViewCell.swift
//  UNASKost
//
//  Created by MacBook on 22/06/21.
//

import UIKit

class ReportTableViewCell: UITableViewCell {

    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var reportTitleValueLabel: UILabel!
    @IBOutlet weak var reportImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageContainerView.roundCorner(radius: imageContainerView.bounds.height / 2)
    }

}
