//
//  ReportDetailTableViewCell.swift
//  UNASKost
//
//  Created by MacBook on 22/06/21.
//

import UIKit

class ReportDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var totalDataValueLabel: UILabel!
    @IBOutlet weak var bubbleSortTimeValueLabel: UILabel!
    @IBOutlet weak var selectionSortTimeValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
