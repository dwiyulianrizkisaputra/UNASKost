//
//  SearchResultTableViewCell.swift
//  UNASKost
//
//  Created by MacBook on 18/06/21.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {

    var boardingHouseData: SearchResultBoardingHouse?{
        didSet{
            setData()
        }
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var maleSignContainerView: UIView!
    @IBOutlet weak var femaleSignContainerView: UIView!
    @IBOutlet weak var statusValueLabel: UILabel!
    @IBOutlet weak var statusBadgeImageView: UIImageView!
    @IBOutlet weak var placePositionLabel: UILabel!
    @IBOutlet weak var placeNameValueLabel: UILabel!
    @IBOutlet weak var distanceToCampusValueLabel: UILabel!
    @IBOutlet weak var totalFacilityValueLabel: UILabel!
    @IBOutlet weak var totalAreaValueLabel: UILabel!
    @IBOutlet weak var priceValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
    }
    
    private func setupViews(){
        containerView.roundCorner(radius: 8)
        containerView.setShadow()
        placeImageView.roundCorner(radius: 8)
        placePositionLabel.isHidden = true
    }
    
    private func setData(){
        guard let data = self.boardingHouseData else {return}
        
        let numberFormatter = NumberFormatter()
        
        let type = data.type
        let rank = data.rank
        let name = data.placeName
        let distance = Double(data.distance) ?? 0
        let totalFacility = data.facility.count
        let totalArea = Double(data.areaSize) ?? 0
        let price = Double(data.price) ?? 0
        let imageUrl = data.image[0].url
        
        if type == "Putra"{
            femaleSignContainerView.isHidden = true
        }else if type == "Putri"{
            maleSignContainerView.isHidden = true
        }
        
        if rank == 1 {
            statusBadgeImageView.image = #imageLiteral(resourceName: "icGold")
        }else if rank == 2{
            statusBadgeImageView.image = #imageLiteral(resourceName: "icSilver")
        }else if rank == 3{
            statusBadgeImageView.image = #imageLiteral(resourceName: "icBronze")
        }else{
            statusBadgeImageView.image = nil
            placePositionLabel.isHidden = false
            placePositionLabel.text = "\(rank)"
        }
        
        self.placeNameValueLabel.text = name
        self.distanceToCampusValueLabel.text = "\(numberFormatter.toDisplayFormat(value: distance)) m"
        self.totalFacilityValueLabel.text = "\(totalFacility)"
        self.totalAreaValueLabel.text = "\(numberFormatter.toDisplayFormat(value: totalArea)) m²"
        self.priceValueLabel.text = "Rp \(numberFormatter.toDisplayFormat(value: price)) / bln"
        
        if let url = URL(string: imageUrl) {
            placeImageView.loadImage(url: url)
        }
        
    }
    
}
