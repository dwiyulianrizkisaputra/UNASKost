//
//  ReportComparationController.swift
//  UNASKost
//
//  Created by MacBook on 24/06/21.
//

import UIKit
import CoreData

typealias reportComparisonListCompletion = ((_ success: Bool, _ listData: [ReportComparation]?) -> Void)

class ReportComparationController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func getItemByCategory(category: SearchCategoryModel, completion: @escaping reportComparisonListCompletion){
        do{
            
            let request = ReportComparation.fetchRequest() as NSFetchRequest<ReportComparation>
            
            let pred = NSPredicate(format: "searchCategory CONTAINS '\(category.text)'")
            let sortTotalData = NSSortDescriptor(key: "totalData", ascending: true)
            let sortMethod = NSSortDescriptor(key: "searchMethod", ascending: true)
            
            request.predicate = pred
            request.sortDescriptors = [sortTotalData, sortMethod]
            
            
            let items = try context.fetch(request)
            
            completion(true, items)
        }
        catch{
            // Error
        }
    }
    
    func createItem(param: CreateReportComparation){
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ReportComparation")
        let categoryPred = NSPredicate(format: "searchCategory = '\(param.category.text)'")
        let totalPred = NSPredicate(format: "totalData = '\(param.totalData)'")
        let methodPred = NSPredicate(format: "searchMethod = '\(param.method)'")
        
        let compoundPred = NSCompoundPredicate(type: .and, subpredicates: [categoryPred, totalPred, methodPred])
        request.predicate = compoundPred
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        
        do {
            try context.execute(deleteRequest)
        }catch{
            //error
        }
        
        let newItem = ReportComparation(context: context)
        newItem.searchCategory = param.category.text
        newItem.searchMethod = param.method
        newItem.searchTime = param.time
        newItem.totalData = param.totalData
        
        do {
            try context.save()
            //getAllItem(category: param.category)
        }
        catch{
            //Error
        }
    }
    
    func deleteItemByCategory(category: SearchCategoryModel){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ReportComparation")
        let categoryPred = NSPredicate(format: "searchCategory = '\(category.text)'")
        request.predicate = categoryPred
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
        
        do {
            try context.execute(deleteRequest)
        }catch{
            //error
        }
    }
    
    func deleteItem(param: CreateReportComparation){
        let item = ReportComparation(context: context)
        item.searchCategory = param.category.text
        item.searchMethod = param.method
        item.searchTime = param.time
        item.totalData = param.totalData
        
        context.delete(item)
        
        do {
            try context.save()
            //getAllItem(category: param.category)
        }
        catch{
            
        }
    }
    
    func updateItem(item: ReportComparation, param: CreateReportComparation){
        item.searchCategory = param.category.text
        item.searchMethod = param.method
        item.searchTime = param.time
        item.totalData = param.totalData
        
        do {
            try context.save()
            //getAllItem(category: param.category)
        }
        catch{
            
        }
        
    }
    
}
