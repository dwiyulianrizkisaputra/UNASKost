//
//  BoardingHouseSearchController.swift
//  UNASKost
//
//  Created by MacBook on 18/06/21.
//

import Foundation

typealias boardingHouseListCompletion = ((_ success: Bool, _ boardingHouseList: [SearchResultBoardingHouse]?) -> Void)

class BoardingHouseSearchController{
    
    let reportComparation = ReportComparationController()
    
    func getBoardingHouseList(param: SearchParamModel, completion: @escaping boardingHouseListCompletion){
        
        var boardingHouseList = [SearchResultBoardingHouse]()
        
        boardingHouseList = self.getDummyData()
        boardingHouseList = self.filterData(param: param, data: boardingHouseList)
        
        if param.searchMethod == .bubbleSort{
            boardingHouseList = self.useBubbleSortMethod(category: param.category, data: boardingHouseList)
        }else{
            boardingHouseList = self.useSelectionSortMethod(category: param.category, data: boardingHouseList)
        }
        
        for i in 0...boardingHouseList.count - 1{
            boardingHouseList[i].rank = i + 1
        }
        
        completion(true, boardingHouseList)
    }
    
    func filterData(param: SearchParamModel, data: [SearchResultBoardingHouse])-> [SearchResultBoardingHouse]{
        
        var filteredData = data
        
        let type = param.type
        let keepPet = param.keepPet
        
        if type == .mixed{
            filteredData = filteredData.filter { $0.type == "Campur" }
        }else if type == .boy{
            filteredData = filteredData.filter { $0.type == "Putra" }
        }else if type == .girl{
            filteredData = filteredData.filter { $0.type == "Putri" }
        }
        
        if keepPet == .allow{
            filteredData = filteredData.filter { $0.keepPetAllowed == true}
        }else if keepPet == .deny{
            filteredData = filteredData.filter { $0.keepPetAllowed == false}
        }
        
        return filteredData
    }
    
    func useBubbleSortMethod(category: SearchCategoryModel, data: [SearchResultBoardingHouse])-> [SearchResultBoardingHouse]{
        guard data.count > 1 else {
            let param = CreateReportComparation(category: category, method: "bubbleSort", time: 0, totalData: Int16(data.count))
            
            reportComparation.createItem(param: param)
            
            return data
            
        }
        
        var sortedData = data
        
        let beginProcess = Date()
        
        switch category {
        case .byPrice:
            for i in 0..<sortedData.count{
                for j in 0..<sortedData.count-i-1{
                    if Double(sortedData[j].price) ?? 0 > Double(sortedData[j + 1].price) ?? 0{
                        sortedData.swapAt(j + 1, j)
                    }
                }
            }
        case .byDistance:
            for i in 0..<sortedData.count{
                for j in 0..<sortedData.count-i-1{
                    if Double(sortedData[j].distance) ?? 0 > Double(sortedData[j + 1].distance) ?? 0{
                        sortedData.swapAt(j + 1, j)
                    }
                }
            }
        case .byTotalFacility:
            for i in 0..<sortedData.count{
                for j in 0..<sortedData.count-i-1{
                    if sortedData[j].facility.count < sortedData[j + 1].facility.count{
                        sortedData.swapAt(j + 1, j)
                    }
                }
            }
        case .byTotalArea:
            for i in 0..<sortedData.count{
                for j in 0..<sortedData.count-i-1{
                    if Double(sortedData[j].areaSize) ?? 0 < Double(sortedData[j + 1].areaSize) ?? 0{
                        sortedData.swapAt(j + 1, j)
                    }
                }
            }
        }
        
        let endProcess = Date()
        let diffTime = Calendar.current.dateComponents([.nanosecond], from: beginProcess, to: endProcess)
        let time = Double(diffTime.nanosecond ?? 0) / 1000000
        
        let param = CreateReportComparation(category: category, method: "bubbleSort", time: time, totalData: Int16(data.count))
        
        reportComparation.createItem(param: param)

        return sortedData
        
    }
    
    func useSelectionSortMethod(category: SearchCategoryModel, data: [SearchResultBoardingHouse])-> [SearchResultBoardingHouse]{
        guard data.count > 1 else {
            let param = CreateReportComparation(category: category, method: "selectionSort", time: 0.0, totalData: Int16(data.count))
            
            reportComparation.createItem(param: param)
            
            return data
        }
        
        var sortedData = data
        
        let beginProcess = Date()
        
        switch category {
        case .byPrice:
            for i in 0..<sortedData.count-1 {
                var lowest = i
                for j in i+1 ..< data.count {
                    if Double(sortedData[j].price) ?? 0 < Double(sortedData[lowest].price) ?? 0{
                        lowest = j
                    }
                }
                if i != lowest {
                    sortedData.swapAt(i, lowest)
                }
            }
        case .byDistance:
            for i in 0..<sortedData.count-1 {
                var lowest = i
                for j in i+1 ..< data.count {
                    if Double(sortedData[j].distance) ?? 0 < Double(sortedData[lowest].distance) ?? 0{
                        lowest = j
                    }
                }
                if i != lowest {
                    sortedData.swapAt(i, lowest)
                }
            }
        case .byTotalFacility:
            for i in 0..<sortedData.count-1 {
                var lowest = i
                for j in i+1 ..< data.count {
                    if sortedData[j].facility.count > sortedData[lowest].facility.count{
                        lowest = j
                    }
                }
                if i != lowest {
                    sortedData.swapAt(i, lowest)
                }
            }
        case .byTotalArea:
            for i in 0..<sortedData.count-1 {
                var lowest = i
                for j in i+1 ..< data.count {
                    if Double(sortedData[j].areaSize) ?? 0 > Double(sortedData[lowest].areaSize) ?? 0{
                        lowest = j
                    }
                }
                if i != lowest {
                    sortedData.swapAt(i, lowest)
                }
            }
        }
        
        let endProcess = Date()
        let diffTime = Calendar.current.dateComponents([.nanosecond], from: beginProcess, to: endProcess)
        //let time = Double(diffTime.second ?? 0)
        let time = Double(diffTime.nanosecond ?? 0) / 1000000
        
        let param = CreateReportComparation(category: category, method: "selectionSort", time: time, totalData: Int16(data.count))
        
        reportComparation.createItem(param: param)
        
        
        return sortedData
        
    }
    
    
    func getDummyData()-> [SearchResultBoardingHouse]{
        let data = [
            SearchResultBoardingHouse(
                id: "001",
                type: "Putri",
                rank: 1,
                placeName: "Kost Singgahsini Purple Ungu",
                distance: "2100",
                areaSize: "13.5",
                price: "1820000",
                longitude: 106.83125621801977,
                latitude: -6.2874485048016355,
                keepPetAllowed: false,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-04/XuoyafzV-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-04/ytECgf1h-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-04/f0o1y9yY-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "TV",
                            imageUrl: "https://static.mamikos.com/uploads/tags/PYPCx8CW.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "002",
                type: "Putra",
                rank: 2,
                placeName: "Kost Ketapang 3B",
                distance: "600",
                areaSize: "42075",
                price: "800000",
                longitude: 106.835338,
                latitude: -6.281489,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2017-05-02/SEgwGAvW-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2017-05-02/Ow02Gjd0-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2017-05-02/UWKNO2Oz-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2017-05-02/x3oYcv3X-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Mesin Cuci",
                            imageUrl: "https://static.mamikos.com/uploads/tags/nFsJ8SPt.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Wifi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/HAAjIo8D.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "TV",
                            imageUrl: "https://static.mamikos.com/uploads/tags/PYPCx8CW.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Kulkas",
                            imageUrl: "https://static.mamikos.com/uploads/tags/2j8mPZpr.png"),
                           BoardingHouseFacility(
                            id: "007",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "003",
                type: "Campur",
                rank: 3,
                placeName: "Salihara Kost Tipe A",
                distance: "140",
                areaSize: "12",
                price: "1200000",
                longitude: 106.83843361040496,
                latitude: -6.2809528421940115,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-01/sqMCULMD-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-01/wz4Y5JIq-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-01/em00jBFr-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-01/7dVPE4R1-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Wifi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/HAAjIo8D.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "TV",
                            imageUrl: "https://static.mamikos.com/uploads/tags/PYPCx8CW.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Meja",
                            imageUrl: "https://static.mamikos.com/uploads/tags/nluOWsFG.png"),
                           BoardingHouseFacility(
                            id: "007",
                            name: "Kursi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MB9milt9.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "009",
                            name: "CCTV",
                            imageUrl: "https://static.mamikos.com/uploads/tags/S0jd2CUw.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Laundry",
                            imageUrl: "https://static.mamikos.com/uploads/tags/xClUcTjt.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "004",
                type: "Campur",
                rank: 4,
                placeName: "Kost Bu Jarwo Tipe B",
                distance: "1100",
                areaSize: "10.5",
                price: "1000000",
                longitude: 106.8416456216489,
                latitude: -6.28582214219392,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2019-10-23/WM2CHOPF-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-10-23/ebNTjCwO-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-10-23/wgiVPfV5-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-10-23/6ILBEJxC-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Wifi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/HAAjIo8D.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Kulkas",
                            imageUrl: "https://static.mamikos.com/uploads/tags/2j8mPZpr.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Meja",
                            imageUrl: "https://static.mamikos.com/uploads/tags/nluOWsFG.png"),
                           BoardingHouseFacility(
                            id: "007",
                            name: "Laundry",
                            imageUrl: "https://static.mamikos.com/uploads/tags/xClUcTjt.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "005",
                type: "Putri",
                rank: 0,
                placeName: "Kost Bambu Kuning Tipe A",
                distance: "650",
                areaSize: "14",
                price: "1500000",
                longitude: 106.83539178990992,
                latitude: -6.28249516262102,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2019-01-25/Ijj82A8x-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-01-25/p0Ud6phE-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-01-25/6F51Pzev-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-01-25/yiHPioUF-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-06-07/q5dhO4He-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-06-07/OX5sJKZh-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Wifi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/HAAjIo8D.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Meja Rias",
                            imageUrl: "https://static.mamikos.com/uploads/tags/ezRch1Ia.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Meja",
                            imageUrl: "https://static.mamikos.com/uploads/tags/nluOWsFG.png"),
                           BoardingHouseFacility(
                            id: "007",
                            name: "Kursi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MB9milt9.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Bantal",
                            imageUrl: "https://static.mamikos.com/uploads/tags/EXwZzFIs.png"),
                           BoardingHouseFacility(
                            id: "007",
                            name: "Guling",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IOJ828wX.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "009",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Rice Cooker",
                            imageUrl: "https://static.mamikos.com/uploads/tags/AKPidgM4.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Dapur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MJ4jiR3v.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Shower",
                            imageUrl: "https://static.mamikos.com/uploads/tags/cbxCWmko.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "006",
                type: "Campur",
                rank: 0,
                placeName: "Kost Ragunan Tipe B",
                distance: "4000",
                areaSize: "25",
                price: "4530000",
                longitude: 106.8233141370072,
                latitude: -6.301203819519137,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-24/X1xPZn4W-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-24/BjxjJ5lR-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-24/fbkDhr9m-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-24/hTKlQwGF-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-24/2wJp6bpl-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-24/r0M2Dlvh-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-24/AO2E74D6-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Wastafel",
                            imageUrl: "https://static.mamikos.com/uploads/tags/g9Tj27Jj.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Taman",
                            imageUrl: "https://static.mamikos.com/uploads/tags/eyaXhro9.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Dispenser",
                            imageUrl: "https://static.mamikos.com/uploads/tags/WUUzIBpn.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "009",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Dapur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MJ4jiR3v.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Shower",
                            imageUrl: "https://static.mamikos.com/uploads/tags/cbxCWmko.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Balcon",
                            imageUrl: "https://static.mamikos.com/uploads/tags/OKRxJK8u.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Air Panas",
                            imageUrl: "https://static.mamikos.com/uploads/tags/zKWuh5ma.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "007",
                type: "Campur",
                rank: 0,
                placeName: "Kost Rifa",
                distance: "1400",
                areaSize: "30",
                price: "2500000",
                longitude: 106.83170047588185,
                latitude: -6.286649431985618,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2018-05-04/rcMCUCzs-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-05-04/NlxtjeCt-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-05-04/Y7Irb1ZW-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-08-01/RKmYakBk-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-05-04/5Hl5GdGY-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-05-04/LIhITFpu-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-05-04/hqYxVlU5-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Wastafel",
                            imageUrl: "https://static.mamikos.com/uploads/tags/g9Tj27Jj.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Taman",
                            imageUrl: "https://static.mamikos.com/uploads/tags/eyaXhro9.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Bantal",
                            imageUrl: "https://static.mamikos.com/uploads/tags/EXwZzFIs.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Guling",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IOJ828wX.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Dispenser",
                            imageUrl: "https://static.mamikos.com/uploads/tags/WUUzIBpn.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "TV",
                            imageUrl: "https://static.mamikos.com/uploads/tags/ZYjIeqGp.png"),
                           BoardingHouseFacility(
                            id: "009",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Dapur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MJ4jiR3v.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Shower",
                            imageUrl: "https://static.mamikos.com/uploads/tags/cbxCWmko.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Balcon",
                            imageUrl: "https://static.mamikos.com/uploads/tags/OKRxJK8u.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Air Panas",
                            imageUrl: "https://static.mamikos.com/uploads/tags/zKWuh5ma.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "008",
                type: "Campur",
                rank: 0,
                placeName: "Kost Omah Tentrem",
                distance: "500",
                areaSize: "15",
                price: "1900000",
                longitude: 106.83787562269994,
                latitude: -6.284037266602642,
                keepPetAllowed: false,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-12/F5vABoac-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-12/XM34d7ep-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-12/iy4tTH5M-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-12/gO0L836u-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-12/zPUvjfSD-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "Wifi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/HAAjIo8D.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Taman",
                            imageUrl: "https://static.mamikos.com/uploads/tags/eyaXhro9.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Bantal",
                            imageUrl: "https://static.mamikos.com/uploads/tags/EXwZzFIs.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Kursi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MB9milt9.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Dispenser",
                            imageUrl: "https://static.mamikos.com/uploads/tags/WUUzIBpn.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "TV",
                            imageUrl: "https://static.mamikos.com/uploads/tags/ZYjIeqGp.png"),
                           BoardingHouseFacility(
                            id: "009",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Dapur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MJ4jiR3v.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Shower",
                            imageUrl: "https://static.mamikos.com/uploads/tags/cbxCWmko.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "CCTV",
                            imageUrl: "https://static.mamikos.com/uploads/tags/S0jd2CUw.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Air Panas",
                            imageUrl: "https://static.mamikos.com/uploads/tags/zKWuh5ma.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Cleaning Service",
                            imageUrl: "https://static.mamikos.com/uploads/tags/1Z9zjxwT.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Rice Cooker",
                            imageUrl: "https://static.mamikos.com/uploads/tags/AKPidgM4.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "009",
                type: "Campur",
                rank: 0,
                placeName: "Kost Damarsari 20",
                distance: "1300",
                areaSize: "9",
                price: "1650000",
                longitude: 106.83177701247284,
                latitude: -6.285297427807309,
                keepPetAllowed: false,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-25/fcxkyQ9P-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-25/lu1mMM7O-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-25/YcYyiJHM-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-25/ulSUMaS1-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-25/N4FTYdRE-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-25/seBVjlgp-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "003",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "009",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Bantal",
                            imageUrl: "https://static.mamikos.com/uploads/tags/EXwZzFIs.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Guling",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IOJ828wX.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Dispenser",
                            imageUrl: "https://static.mamikos.com/uploads/tags/WUUzIBpn.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "Kulkas",
                            imageUrl: "https://static.mamikos.com/uploads/tags/2j8mPZpr.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Dapur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MJ4jiR3v.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Balcon",
                            imageUrl: "https://static.mamikos.com/uploads/tags/OKRxJK8u.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "010",
                type: "Putri",
                rank: 0,
                placeName: "Kost Omah Eyang Tipe A",
                distance: "1500",
                areaSize: "9",
                price: "2500000",
                longitude: 106.83123461621972,
                latitude: -6.2874428441911085,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2020-07-30/fKyUQWuh-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-11-19/OTWopajX-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-11-19/Tio3osMc-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-11-19/Yfm22VPz-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-11-19/XmzLN9fJ-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-07-30/fKyUQWuh-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-07-30/CRh8ult7-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Shower",
                            imageUrl: "https://static.mamikos.com/uploads/tags/cbxCWmko.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Cleaning Service",
                            imageUrl: "https://static.mamikos.com/uploads/tags/1Z9zjxwT.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Wifi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/HAAjIo8D.png"),
                           BoardingHouseFacility(
                            id: "006",
                            name: "Dispenser",
                            imageUrl: "https://static.mamikos.com/uploads/tags/WUUzIBpn.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "TV",
                            imageUrl: "https://static.mamikos.com/uploads/tags/ZYjIeqGp.png"),
                           BoardingHouseFacility(
                            id: "009",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Dapur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MJ4jiR3v.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Kulkas",
                            imageUrl: "https://static.mamikos.com/uploads/tags/2j8mPZpr.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Mesin Cuci",
                            imageUrl: "https://static.mamikos.com/uploads/tags/nFsJ8SPt.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Laundry",
                            imageUrl: "https://static.mamikos.com/uploads/tags/xClUcTjt.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Air Panas",
                            imageUrl: "https://static.mamikos.com/uploads/tags/zKWuh5ma.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "011",
                type: "Putri",
                rank: 0,
                placeName: "Kost H Husein",
                distance: "1600",
                areaSize: "4",
                price: "650000",
                longitude: 106.83060824203005,
                latitude: -6.287961617961935,
                keepPetAllowed: false,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2020-12-01/MAdgPdLT-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-12-01/B0Pb8Wea-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-12-01/eWyzwapd-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-12-01/IdgotNio-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-12-01/c5D4mi4R-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-12-01/6iRFYDd3-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Bantal",
                            imageUrl: "https://static.mamikos.com/uploads/tags/EXwZzFIs.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Guling",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IOJ828wX.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Dapur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/MJ4jiR3v.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Jemuran",
                            imageUrl: "https://static.mamikos.com/uploads/tags/gqKbMVTQ.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "012",
                type: "Putri",
                rank: 0,
                placeName: "Kost Savitra",
                distance: "1400",
                areaSize: "9",
                price: "800000",
                longitude: 106.84064664866132,
                latitude: -6.288947579576831,
                keepPetAllowed: false,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2020-07-24/jZ7Goo0Q-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-04/fFMMC6nd-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-04/4en7QKKz-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-04/ij0eW8RD-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-04/3ARF5s4P-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "006",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2019-09-04/NxY3N2wH-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "010",
                            name: "Shower",
                            imageUrl: "https://static.mamikos.com/uploads/tags/cbxCWmko.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "Cermin",
                            imageUrl: "https://static.mamikos.com/uploads/tags/dzlnJlHi.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "Kipas Angin",
                            imageUrl: "https://static.mamikos.com/uploads/tags/gQ6hjpk3.png"),
                           BoardingHouseFacility(
                            id: "009",
                            name: "Kloset Duduk",
                            imageUrl: "https://static.mamikos.com/uploads/tags/TyT47meW.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "013",
                type: "Campur",
                rank: 0,
                placeName: "Kost Mami Zainab Tipe A",
                distance: "850",
                areaSize: "16",
                price: "800000",
                longitude: 106.83426475163317,
                latitude: -6.278190395919311,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-11/iNWAaCU6-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-11/5ZwajmO5-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-11/O4vKJ8mR-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-11/AM1kywrI-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-03-11/fbSJ83L7-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Ember Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/B71K93ll.png"),
                ]
            ),
            SearchResultBoardingHouse(
                id: "014",
                type: "Campur",
                rank: 0,
                placeName: "Kost Pak Herman",
                distance: "1800",
                areaSize: "12",
                price: "800000",
                longitude: 106.83026673510037,
                latitude: -6.2890702169611075,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2018-02-17/mPFsxoWI-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-02-17/Go1cb19h-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-02-17/8iJ93h6f-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2018-02-17/kMayTEDU-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2017-10-24/ujSfdjB0-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "002",
                            name: "Kasur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/Y4ZfXUdX.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Lemari",
                            imageUrl: "https://static.mamikos.com/uploads/tags/egu43HCg.png"),
                           BoardingHouseFacility(
                            id: "004",
                            name: "Ruang Jemur",
                            imageUrl: "https://static.mamikos.com/uploads/tags/9sih6A2l.png"),
                           BoardingHouseFacility(
                            id: "011",
                            name: "Parkir",
                            imageUrl: "https://static.mamikos.com/uploads/tags/XaqV0PnZ.png")
                ]
            ),
            SearchResultBoardingHouse(
                id: "015",
                type: "Campur",
                rank: 0,
                placeName: "Kost Pola 27",
                distance: "1400",
                areaSize: "12",
                price: "1800000",
                longitude: 106.83223784896651,
                latitude: -6.2879121549008925,
                keepPetAllowed: true,
                image: [BoardingHouseImage(
                            id: "001",
                            url: "https://static.mamikos.com/uploads/cache/data/style/2020-06-29/Nhi5SxOr-540x720.jpg",
                            isMain: true),
                        BoardingHouseImage(
                                    id: "002",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-06-29/zkcddAJ3-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "003",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-09/LQRA41N2-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "004",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2020-06-29/Y5a12Ud2-540x720.jpg",
                                    isMain: false),
                        BoardingHouseImage(
                                    id: "005",
                                    url: "https://static.mamikos.com/uploads/cache/data/style/2021-06-09/9z1dG7GV-540x720.jpg",
                                    isMain: false)
                ],
                facility: [BoardingHouseFacility(
                            id: "001",
                            name: "K. Mandi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/IAZ2n9ql.png"),
                           BoardingHouseFacility(
                            id: "005",
                            name: "Wifi",
                            imageUrl: "https://static.mamikos.com/uploads/tags/HAAjIo8D.png"),
                           BoardingHouseFacility(
                            id: "008",
                            name: "AC",
                            imageUrl: "https://static.mamikos.com/uploads/tags/lqunlyi8.png")
                ]
            )
        ]
        return data
    }
    
}
