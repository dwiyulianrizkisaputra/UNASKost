//
//  SearchResultDetailViewController.swift
//  UNASKost
//
//  Created by MacBook on 19/06/21.
//

import UIKit
import GoogleMaps
import FSPagerView

class SearchResultDetailViewController: BaseViewController {

    var boardingHouseData: SearchResultBoardingHouse?
    
    private let imageSliderCellId = "search_result_detail_slider_cell"
    
    @IBOutlet weak var imageSliderView: FSPagerView!
    @IBOutlet weak var imageSliderIndicator: FSPageControl!
    @IBOutlet weak var placeholderImageContainerView: UIView!
    @IBOutlet weak var placeNameValueLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var contactContainerView: UIView!
    @IBOutlet weak var maleSignContainerView: UIView!
    @IBOutlet weak var femaleSignContainerView: UIView!
    @IBOutlet weak var distanceToCampusValueLabel: UILabel!
    @IBOutlet weak var totalFacilityValueLabel: UILabel!
    @IBOutlet weak var totalAreaValueLabel: UILabel!
    @IBOutlet weak var listFacilityStackView: UIStackView!
    @IBOutlet weak var listFacilityHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rulesTapAreaView: UIView!
    @IBOutlet weak var additionalInfoTapAreaView: UIView!
    @IBOutlet weak var mapContainerView: GMSMapView!
    @IBOutlet weak var getCurrentLocationView: UIView!
    @IBOutlet weak var priceContainerView: UIView!
    @IBOutlet weak var priceValueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showTitle(text: "Kost Detail")
        showBackButton()
        setupViews()
        setData()
        setupImageSlider()
        setupMapVew()
    }
    
    private func setupViews(){
        contactContainerView.roundCorner(radius: contactContainerView.bounds.height / 2)
        contactContainerView.layer.borderWidth = 1
        contactContainerView.layer.borderColor = UIColor.gray.withAlphaComponent(0.1).cgColor
        priceContainerView.setShadow(type: .top)
        mapContainerView.roundCorner(radius: 12)
        getCurrentLocationView.roundCorner(radius: getCurrentLocationView.bounds.height / 2)
        
        favoriteButton.addTarget(self, action: #selector(didTappedFavorite), for: .touchUpInside)
        
        let contactOwnerTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTappedContactOwner))
        let rulesTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTappedRules))
        let additionalInfoTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTappedAdditionalInfo))
        let getCurrentLocationTapGesture = UITapGestureRecognizer(target: self, action: #selector(didTappedGetCurrentLocation))
        
        contactContainerView.addGestureRecognizer(contactOwnerTapGesture)
        rulesTapAreaView.addGestureRecognizer(rulesTapGesture)
        additionalInfoTapAreaView.addGestureRecognizer(additionalInfoTapGesture)
        getCurrentLocationView.addGestureRecognizer(getCurrentLocationTapGesture)
    }
    
    private func setData(){
        guard let data = self.boardingHouseData else {return}
        
        let numberFormatter = NumberFormatter()
        
        let placeName = data.placeName
        let type = data.type
        let distance = Double(data.distance) ?? 0
        let totalFacility = data.facility.count
        let listFacility = data.facility
        let totalArea = Double(data.areaSize) ?? 0
        let price = Double(data.price) ?? 0
        
        if type == "Putra"{
            femaleSignContainerView.isHidden = true
        }else if type == "Putri"{
            maleSignContainerView.isHidden = true
        }
        
        placeNameValueLabel.text = placeName
        distanceToCampusValueLabel.text = "\(numberFormatter.toDisplayFormat(value: distance)) m"
        totalFacilityValueLabel.text = "\(totalFacility)"
        totalAreaValueLabel.text = "\(numberFormatter.toDisplayFormat(value: totalArea)) m²"
        priceValueLabel.text = "Rp \(numberFormatter.toDisplayFormat(value: price)) / bln"
        
        setFacilityData(listFacility)
    }
    
    private func setFacilityData(_ data: [BoardingHouseFacility]){
        self.listFacilityStackView.subviews.forEach({ (view) in
            view.removeFromSuperview()
        })
        let totalData = data.count
        
        let totalFacility = (Double(totalData) / 2).rounded(.toNearestOrAwayFromZero)
        var no = 0
        
        for _ in 0...Int(totalFacility) - 1 {
            var groupedFacilities = [BoardingHouseFacility]()
            
            if totalData == 1{
                groupedFacilities.append(data[0])
            }else{
                if no + 1 <= totalData - 1 {
                    groupedFacilities.append(data[no])
                    groupedFacilities.append(data[no + 1])
                    no += 2
                }else{
                    if no <= totalData - 1 {
                        groupedFacilities.append(data[no])
                    }
                }
            }
            
            let facilityStackView = ListFacilitiesStackView()
            facilityStackView.content = groupedFacilities
            
            
            listFacilityStackView.addArrangedSubview(facilityStackView)
            
        }
        
        listFacilityHeightConstraint.constant = CGFloat(totalFacility * 32)
        
    }
    
    private func setupImageSlider() {
        
        guard let data = self.boardingHouseData else {return}
        
        imageSliderView.delegate = self
        imageSliderView.dataSource = self
        imageSliderView.isInfinite = true
        
        let totalImage = data.image.count
        
        if totalImage > 0 {
            placeholderImageContainerView.isHidden = true
        }
        
        imageSliderIndicator.numberOfPages = totalImage
        imageSliderIndicator.contentHorizontalAlignment = .left
        imageSliderIndicator.contentInsets = UIEdgeInsets(top: 12, left: 16, bottom: 0, right: 0)
        
        imageSliderIndicator.setFillColor(Color.shared.dark.withAlphaComponent(0.44), for: .normal)
        imageSliderIndicator.setFillColor(Color.shared.greenPrimary, for: .selected)
        
        
        imageSliderIndicator.itemSpacing = 10
        let normalSize = 8
        
        imageSliderIndicator.setPath(UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: normalSize, height: normalSize), cornerRadius: 4), for: .normal)
        
        imageSliderIndicator.setPath(UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: normalSize, height: normalSize), cornerRadius: 4), for: .selected)
        
        imageSliderView.register(
            UINib(nibName: "SearchResultDetailImageSliderCell", bundle: nil),
            forCellWithReuseIdentifier: imageSliderCellId
        )
    }
    
    private func setupMapVew(){
        guard let data = self.boardingHouseData else {return}
        
        let latitude = data.latitude
        let longitude = data.longitude
        
        let destinationMarker = GMSMarker()
        destinationMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        destinationMarker.icon = GMSMarker.markerImage(with: .yellow)
        destinationMarker.map = self.mapContainerView
        
        let camera = GMSCameraPosition(target: destinationMarker.position, zoom: 15)
        self.mapContainerView.animate(to: camera)
    }
    
    @objc
    private func didTappedFavorite(){
        self.showAlert(message: "Favorite!")
    }
    
    @objc
    private func didTappedContactOwner(){
        self.showAlert(message: "Contact Owner!")
    }
    
    @objc
    private func didTappedRules(){
        self.showAlert(message: "Rules!")
    }
    
    @objc
    private func didTappedAdditionalInfo(){
        self.showAlert(message: "Additional Info!")
    }
    
    @objc
    private func didTappedGetCurrentLocation(){
        guard let data = self.boardingHouseData else {return}
        
        let latitude = data.latitude
        let longitude = data.longitude
        
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        let camera = GMSCameraPosition(target: sourceMarker.position, zoom: 15)
        self.mapContainerView.animate(to: camera)
    }

}

extension SearchResultDetailViewController: FSPagerViewDelegate, FSPagerViewDataSource{
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.boardingHouseData?.image.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: imageSliderCellId, at: index) as! SearchResultDetailImageSliderCell
        
        if let data = self.boardingHouseData{
            let imageUrl = data.image[index].url
            cell.imageUrl = imageUrl
        }
        
        return cell
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        imageSliderIndicator.currentPage = pagerView.currentIndex
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        imageSliderIndicator.currentPage = targetIndex
    }
    
    
}
