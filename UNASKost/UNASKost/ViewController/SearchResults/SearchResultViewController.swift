//
//  SearchResultViewController.swift
//  UNASKost
//
//  Created by MacBook on 18/06/21.
//

import UIKit

class SearchResultViewController: BaseViewController {

    var searchParam: SearchParamModel?
    var searchResultData = [SearchResultBoardingHouse]()
    
    private let cellIdentifier = "search_result_cell"
    private var controller = BoardingHouseSearchController()
    
    @IBOutlet weak var searchResultTableView: UITableView!{
        didSet{
            searchResultTableView.dataSource = self
            searchResultTableView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showBackButton()
        
        var navTitle = ""
        
        if let param = searchParam {
            switch param.category {
            case .byPrice:
                navTitle = "Kategori Harga"
            case .byDistance:
                navTitle = "Kategori Jarak Ke Kampus"
            case .byTotalFacility:
                navTitle = "Kategori Jumlah Fasilitas"
            case .byTotalArea:
                navTitle = "Kategori Luas Tempat"
            }
        }
        
        showTitle(text: navTitle)
        getListBoardingHouse()
    }
    
    private func getListBoardingHouse(){
        
        guard let param = self.searchParam else {return}
        
        DispatchQueue.global().async {
            self.controller.getBoardingHouseList(param: param, completion: { success, boardingHouseList in
                if success {
                    guard let data = boardingHouseList else { return }
                    self.searchResultData = data
                    
                    DispatchQueue.main.async {
                        self.searchResultTableView.reloadData()
                    }
                } else {
                    self.showAlert(message: "Failed to get boarding house data")
                }
            })
        }
    }

}

extension SearchResultViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchResultData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SearchResultTableViewCell
            cell.boardingHouseData = self.searchResultData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchResultDetailViewController = UIStoryboard(name: "SearchResult", bundle: nil).instantiateViewController(withIdentifier: "SearchResultDetailVC") as! SearchResultDetailViewController
        searchResultDetailViewController.boardingHouseData = self.searchResultData[indexPath.row]
        
        self.navigationController?.pushViewController(searchResultDetailViewController, animated: true)
        
        
    }
    
    
}
