//
//  SearchFilterViewController.swift
//  UNASKost
//
//  Created by MacBook on 17/06/21.
//

import UIKit

class SearchFilterViewController: BaseViewController {

    var searchCategory: SearchCategoryModel = .byPrice
    private var searchType: SearchTypeModel = .all
    private var searchKeepPet: SearchKeepPetModel = .all
    private var searchMethod: SearchMethodModel = .bubbleSort
    
    @IBOutlet weak var allTypeButton: UIButton!
    @IBOutlet weak var mixedTypeButton: UIButton!
    @IBOutlet weak var boyTypeButton: UIButton!
    @IBOutlet weak var girlTypeButton: UIButton!
    @IBOutlet weak var allKeepPetButton: UIButton!
    @IBOutlet weak var allowKeepPetButton: UIButton!
    @IBOutlet weak var rejectKeepPetButton: UIButton!
    @IBOutlet weak var bubbleSortMethodButton: UIButton!
    @IBOutlet weak var selectionSortMethodButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showCustomBackButton(imageName: "icCloseWhite", action: #selector(didDismiss))
        showTitle(text: "Filter")
        self.setupViews()

    }
    
    private func setupViews(){
        //Register tag
        allTypeButton.tag = 1
        mixedTypeButton.tag = 2
        boyTypeButton.tag = 3
        girlTypeButton.tag = 4
        searchButton.tag = 99
        allKeepPetButton.tag = 11
        allowKeepPetButton.tag = 12
        rejectKeepPetButton.tag = 13
        bubbleSortMethodButton.tag = 20
        selectionSortMethodButton.tag = 30
        
        // Set default radio
        setSelectedButton(allTypeButton)
        setSelectedButton(mixedTypeButton, isSelected: false)
        setSelectedButton(boyTypeButton, isSelected: false)
        setSelectedButton(girlTypeButton, isSelected: false)
        
        setSelectedButton(allKeepPetButton)
        setSelectedButton(allowKeepPetButton, isSelected: false)
        setSelectedButton(rejectKeepPetButton, isSelected: false)
        
        setSelectedButton(bubbleSortMethodButton)
        setSelectedButton(selectionSortMethodButton, isSelected: false)
        
        bubbleSortMethodButton.setTitleColor(.white, for: .normal)
        selectionSortMethodButton.setTitleColor(Color.shared.dark, for: .normal)
        
        searchButton.roundCorner(radius: searchButton.bounds.height / 2)
    }
    
    @IBAction func didTapButton(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            searchType = .all
            setSelectedButton(allTypeButton)
            setSelectedButton(mixedTypeButton, isSelected: false)
            setSelectedButton(boyTypeButton, isSelected: false)
            setSelectedButton(girlTypeButton, isSelected: false)
        case 2:
            searchType = .mixed
            setSelectedButton(mixedTypeButton)
            setSelectedButton(allTypeButton, isSelected: false)
            setSelectedButton(boyTypeButton, isSelected: false)
            setSelectedButton(girlTypeButton, isSelected: false)
        case 3:
            searchType = .boy
            setSelectedButton(boyTypeButton)
            setSelectedButton(allTypeButton, isSelected: false)
            setSelectedButton(mixedTypeButton, isSelected: false)
            setSelectedButton(girlTypeButton, isSelected: false)
        case 4:
            searchType = .girl
            setSelectedButton(girlTypeButton)
            setSelectedButton(allTypeButton, isSelected: false)
            setSelectedButton(mixedTypeButton, isSelected: false)
            setSelectedButton(boyTypeButton, isSelected: false)
        case 11:
            searchKeepPet = .all
            setSelectedButton(allKeepPetButton)
            setSelectedButton(allowKeepPetButton, isSelected: false)
            setSelectedButton(rejectKeepPetButton, isSelected: false)
        case 12:
            searchKeepPet = .allow
            setSelectedButton(allowKeepPetButton)
            setSelectedButton(allKeepPetButton, isSelected: false)
            setSelectedButton(rejectKeepPetButton, isSelected: false)
        case 13:
            searchKeepPet = .deny
            setSelectedButton(rejectKeepPetButton)
            setSelectedButton(allKeepPetButton, isSelected: false)
            setSelectedButton(allowKeepPetButton, isSelected: false)
        case 20:
            searchMethod = .bubbleSort
            setSelectedButton(bubbleSortMethodButton)
            setSelectedButton(selectionSortMethodButton, isSelected: false)
            bubbleSortMethodButton.setTitleColor(.white, for: .normal)
            selectionSortMethodButton.setTitleColor(Color.shared.dark, for: .normal)
        case 30:
            searchMethod = .selectionSort
            setSelectedButton(selectionSortMethodButton)
            setSelectedButton(bubbleSortMethodButton, isSelected: false)
            selectionSortMethodButton.setTitleColor(.white, for: .normal)
            bubbleSortMethodButton.setTitleColor(Color.shared.dark, for: .normal)
        case 99:
            
            let param = SearchParamModel(
                category: searchCategory,
                type: searchType,
                keepPet: searchKeepPet,
                searchMethod: searchMethod)
            
            
            let searchResultViewController = UIStoryboard(name: "SearchResult", bundle: nil).instantiateViewController(withIdentifier:  "SearchResultVC") as! SearchResultViewController
            
            searchResultViewController.searchParam = param
            self.navigationController?.pushViewController(
                searchResultViewController,
                animated: true
            )
            
        default:
            break
        }
        
    }
    
    private func setSelectedButton(_ button: UIButton, isSelected: Bool = true){
        button.roundCorner(radius: button.bounds.height / 2)
        button.layer.borderWidth = 2
        button.layer.borderColor = Color.shared.dark.cgColor
        
        
        if isSelected{
            button.backgroundColor = Color.shared.dark
        }else{
            button.backgroundColor = .white
        }
    }
    
    @objc func didDismiss(_ sender: Any) {
        let isRootViewController = navigationController?.viewControllers.count == 1
        if isRootViewController {
            navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        
        navigationController?.popViewController(animated: true)
    }

}
