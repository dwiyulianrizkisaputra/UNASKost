//
//  HomeViewController.swift
//  UNASKost
//
//  Created by MacBook on 02/06/21.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class HomeViewController: UIViewController {

    @IBOutlet weak var profileInfoContainerView: UIView!
    @IBOutlet weak var searchFitureContainerView: UIView!
    @IBOutlet weak var navigationToCampusContainerView: UIView!
    @IBOutlet weak var priceContainerView: UIView!
    @IBOutlet weak var distanceContainerView: UIView!
    @IBOutlet weak var totalFacilityContainerView: UIView!
    @IBOutlet weak var totalAreaContainerView: UIView!
    @IBOutlet weak var mapDirectionContainerView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
        self.setupMapView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.hideNavigation()
    }
    
    private func hideNavigation(){
        self.navigationController?.isNavigationBarHidden = true
    }

    private func setupViews(){
        self.searchFitureContainerView.roundCorner(radius: 20, type: .topOnly)
        self.navigationToCampusContainerView.roundCorner(radius: 20, type: .topOnly)
        self.mapDirectionContainerView.roundCorner(radius: 12)
        self.mapDirectionContainerView.isUserInteractionEnabled = false

        self.priceContainerView.layer.borderWidth = 1
        self.priceContainerView.layer.borderColor = UIColor.white.cgColor
        self.priceContainerView.roundCorner(radius: 8)
        self.distanceContainerView.layer.borderWidth = 1
        self.distanceContainerView.layer.borderColor = UIColor.white.cgColor
        self.distanceContainerView.roundCorner(radius: 8)
        self.totalFacilityContainerView.layer.borderWidth = 1
        self.totalFacilityContainerView.layer.borderColor = UIColor.white.cgColor
        self.totalFacilityContainerView.roundCorner(radius: 8)
        self.totalAreaContainerView.layer.borderWidth = 1
        self.totalAreaContainerView.layer.borderColor = UIColor.white.cgColor
        self.totalAreaContainerView.roundCorner(radius: 8)
        self.mapDirectionContainerView.backgroundColor = .gray
        
        
        let priceTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(priceViewDidSelect))
        let distanceTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(distanceViewDidSelect))
        let totalFacilityTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(totalFacilityViewDidSelect))
        let totalAreaTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(totalAreaViewDidSelect))
        
        self.priceContainerView.addGestureRecognizer(priceTapGesture)
        self.distanceContainerView.addGestureRecognizer(distanceTapGesture)
        self.totalFacilityContainerView.addGestureRecognizer(totalFacilityTapGesture)
        self.totalAreaContainerView.addGestureRecognizer(totalAreaTapGesture)
        
    }
    
    private func setupMapView(){
        
        // MARK: Define the source latitude and longitude
        let sourceLat = -6.283425632904959
        let sourceLng = 106.83667582622053
            
        // MARK: Define the destination latitude and longitude
        let destinationLat = -6.2806505610837045
        let destinationLng = 106.83937289731249
        
        // MARK: Create source location and destination location so that you can pass it to the URL
        let sourceLocation = "\(sourceLat),\(sourceLng)"
        let destinationLocation = "\(destinationLat),\(destinationLng)"
                
        // MARK: Create URL
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLocation)&destination=\(destinationLocation)&key=AIzaSyDkx_VccE_D_EtH1EHuxG3oOetgrdcyM8E"

        // MARK: Request for response from the google
        
        AF.request(url).responseJSON { (reseponse) in
            guard let data = reseponse.data else {
                return
            }
            
            do {
                let jsonData = try JSON(data: data)
                let routes = jsonData["routes"].arrayValue
                print("response = \(jsonData)")
                for route in routes {
                    let overview_polyline = route["overview_polyline"].dictionary
                    let points = overview_polyline?["points"]?.string
                    let path = GMSPath.init(fromEncodedPath: points ?? "")
                    let polyline = GMSPolyline.init(path: path)
                    polyline.strokeColor = .systemBlue
                    polyline.strokeWidth = 5
                    polyline.map = self.mapDirectionContainerView
                }
            }
             catch let error {
                print(error.localizedDescription)
            }
        }
        
        // MARK: Marker for source location
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2D(latitude: sourceLat, longitude: sourceLng)
//        sourceMarker.title = "-"
//        sourceMarker.snippet = "-"
        sourceMarker.map = self.mapDirectionContainerView
        
        
        // MARK: Marker for destination location
        let destinationMarker = GMSMarker()
        destinationMarker.position = CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLng)
        destinationMarker.icon = GMSMarker.markerImage(with: Color.shared.greenPrimary)
//        destinationMarker.title = "-"
//        destinationMarker.snippet = "-"
        destinationMarker.map = self.mapDirectionContainerView
        
        let camera = GMSCameraPosition(target: sourceMarker.position, zoom: 15)
        self.mapDirectionContainerView.animate(to: camera)
        
    }
    
    @objc private func priceViewDidSelect(){
        let searchFilterViewController = UIStoryboard(name: "Home", bundle: nil)
            .instantiateViewController(withIdentifier: "SearchFilterVC") as! SearchFilterViewController
        let uiNavigationViewController = BaseNavigationViewController(
            rootViewController: searchFilterViewController
        )
        
        uiNavigationViewController.modalPresentationStyle = .fullScreen
        
        self.present(uiNavigationViewController, animated: true, completion: nil)
    }
    
    @objc private func distanceViewDidSelect(){
        let searchFilterViewController = UIStoryboard(name: "Home", bundle: nil)
            .instantiateViewController(withIdentifier: "SearchFilterVC") as! SearchFilterViewController
        searchFilterViewController.searchCategory = .byDistance
        
        let uiNavigationViewController = BaseNavigationViewController(
            rootViewController: searchFilterViewController
        )
        
        uiNavigationViewController.modalPresentationStyle = .fullScreen
        
        self.present(uiNavigationViewController, animated: true, completion: nil)
    }
    
    @objc private func totalFacilityViewDidSelect(){
        let searchFilterViewController = UIStoryboard(name: "Home", bundle: nil)
            .instantiateViewController(withIdentifier: "SearchFilterVC") as! SearchFilterViewController
        searchFilterViewController.searchCategory = .byTotalFacility
        
        let uiNavigationViewController = BaseNavigationViewController(
            rootViewController: searchFilterViewController
        )
        
        uiNavigationViewController.modalPresentationStyle = .fullScreen
        
        self.present(uiNavigationViewController, animated: true, completion: nil)
    }

    @objc private func totalAreaViewDidSelect(){
        let searchFilterViewController = UIStoryboard(name: "Home", bundle: nil)
            .instantiateViewController(withIdentifier: "SearchFilterVC") as! SearchFilterViewController
        searchFilterViewController.searchCategory = .byTotalArea
        
        let uiNavigationViewController = BaseNavigationViewController(
            rootViewController: searchFilterViewController
        )
        
        uiNavigationViewController.modalPresentationStyle = .fullScreen
        
        self.present(uiNavigationViewController, animated: true, completion: nil)
    }
}

