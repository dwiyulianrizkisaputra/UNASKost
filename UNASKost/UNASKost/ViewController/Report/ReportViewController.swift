//
//  ReportViewController.swift
//  UNASKost
//
//  Created by MacBook on 22/06/21.
//

import UIKit

class ReportViewController: BaseViewController {

    let cellIdentifier = "report_tabe_cell"
    private var reportData = [SearchCategoryModel]()
    
    @IBOutlet weak var reportTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reportData = [.byPrice, .byDistance, .byTotalFacility, .byTotalArea]
        showTitle(text: "Laporan")
        reportTableView.dataSource = self
        reportTableView.delegate = self
    }
    
}

extension ReportViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reportData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ReportTableViewCell
        
        cell.reportTitleValueLabel.text = self.reportData[indexPath.row].title
        cell.reportImageView.image = self.reportData[indexPath.row].image

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let reportDetailViewController = UIStoryboard(name: "Report", bundle: nil).instantiateViewController(withIdentifier: "ReportDetailVC") as! ReportDetailViewController
        reportDetailViewController.category = self.reportData[indexPath.row]
        
        self.navigationController?.pushViewController(reportDetailViewController, animated: true)
    }
}
