//
//  ReportDetailViewController.swift
//  UNASKost
//
//  Created by MacBook on 22/06/21.
//

import UIKit

class ReportDetailViewController: BaseViewController {

    var category: SearchCategoryModel?
    private var resultData = [ResultReportComparation]()
    private let cellIdentifier = "report_detail_table_cell"
    private let reportComparation = ReportComparationController()
    
    @IBOutlet weak var reportDetailTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var headerTitle = ""
        
        if let data = category {
            headerTitle = data.title
        }
        
        showTitle(text: headerTitle)
        showBackButton()
        setupNavigationButton()
        reportDetailTableView.dataSource = self
        reportDetailTableView.delegate = self
        retrieveData()
    }
    
    private func retrieveData(){
        guard let searchCategory = category else {return}
        
        DispatchQueue.global().async {
            self.reportComparation.getItemByCategory(category: searchCategory, completion: { success, listData in
                if success {
                    guard let data = listData else { return }
                    
                    self.resultData = self.grupedData(data: data)
                    
                    DispatchQueue.main.async {
                        self.reportDetailTableView.reloadData()
                    }
                } else {
                    self.showAlert(message: "Failed to get data")
                }
            })
        }
    }
    
    private func grupedData(data:[ReportComparation]) -> [ResultReportComparation] {
        var resultData = [ResultReportComparation]()
        guard data.count > 0 else {return resultData}
        var j:Int16 = 0
        
        print("======")
        
        for i in 0...data.count - 1{
            //let searchMetod = data[i].searchMethod
            let time = data[i].searchTime
            let totalData = data[i].totalData
            
            print(totalData)
            
            if j != data[i].totalData{
                j = data[i].totalData
                
                if data[i].searchMethod == "selectionSort" {
                    resultData.append(ResultReportComparation(totalData: totalData, bubbleSortTime: 0.0, selectionSortTime: time))
                }else{
                    if i + 1 <= data.count - 1 {
                        if data[i].searchMethod == "bubbleSort" && totalData != data[i+1].totalData {
                            resultData.append(ResultReportComparation(totalData: totalData, bubbleSortTime: time, selectionSortTime: 0.0))
                        }
                    }else if i == data.count - 1{
                        resultData.append(ResultReportComparation(totalData: totalData, bubbleSortTime: time, selectionSortTime: 0.0))
                    }                    
                }
            }else{
                if data[i].totalData != 0 {
                    if data[i].searchMethod == "selectionSort" {
                        let bubbleSortTime = data[i - 1].searchTime
                        resultData.append(ResultReportComparation(totalData: totalData, bubbleSortTime: bubbleSortTime, selectionSortTime: time))
                    }
                }
            }
        }
        
        return resultData
    }
    
    private func setupNavigationButton(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(didTapDelete))
        navigationItem.rightBarButtonItem?.tintColor = .white
    }
    
    @objc
    private func didTapDelete(){
        let alert = UIAlertController(title: "Hapus Data", message: "Apakah anda yakin ingin menghapus data?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Batal", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Hapus", style: .destructive, handler: { action in
            guard let category = self.category else {return}
            
            self.reportComparation.deleteItemByCategory(category: category)
            self.retrieveData()
            
        }))
        
        present(alert, animated: true)
    }
    
}

extension ReportDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ReportDetailTableViewCell
        
        let selectedData = self.resultData[indexPath.row]
        
        let totalData = "\(selectedData.totalData)"
        var bubbleSortTime = "\(String(format: "%.2f", selectedData.bubbleSortTime)) ms"
        var selectionSortTime = "\(String(format: "%.2f", selectedData.selectionSortTime)) ms"
        
        if bubbleSortTime == "0.00 ms" {bubbleSortTime = "-"}
        if selectionSortTime == "0.00 ms" {selectionSortTime = "-"}
        
        cell.totalDataValueLabel.text = totalData
        cell.bubbleSortTimeValueLabel.text = bubbleSortTime
        cell.selectionSortTimeValueLabel.text = selectionSortTime
        
        if indexPath.row % 2 == 0{
            cell.contentView.backgroundColor = Color.shared.greenSecondary
        }else{
            cell.contentView.backgroundColor = Color.shared.gray
        }
        
        return cell
    }
    
    
}
