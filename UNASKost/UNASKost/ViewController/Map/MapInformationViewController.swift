//
//  MapInformationViewController.swift
//  UNASKost
//
//  Created by MacBook on 20/06/21.
//

import UIKit

class MapInformationViewController: UIViewController {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.roundCorner(radius: 20)
        closeButton.addTarget(self, action: #selector(didTappedCloseButton), for: .touchUpInside)
    }
    
    @objc
    private func didTappedCloseButton(){
        self.dismiss(animated: true, completion: nil)
    }

}
