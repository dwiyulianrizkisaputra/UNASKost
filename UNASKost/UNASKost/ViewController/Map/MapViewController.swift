//
//  MapViewController.swift
//  UNASKost
//
//  Created by MacBook on 16/06/21.
//

import UIKit
import GoogleMaps

class MapViewController: BaseViewController {

    var searchParam: SearchParamModel?
    var searchResultData = [SearchResultBoardingHouse](){
        didSet{
            self.retrieveMapView()
        }
    }
    private var controller = BoardingHouseSearchController()
    private let currentLat = -6.283425632904959
    private let currentLng = 106.83667582622053
    private let campusLat = -6.2806505610837045
    private let campusLng = 106.83937289731249
    
    @IBOutlet weak var mapContainerView: GMSMapView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var listTapAreaView: UIView!
    @IBOutlet weak var informationTapAreaView: UIView!
    @IBOutlet weak var getCurrentLocationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showTitle(text: "Tempat Disekitar Anda")
        self.searchParam = SearchParamModel(category: .byDistance, type: .all, keepPet: .all, searchMethod: .selectionSort)
        self.setupViews()
        self.setupMapView()
    }

    private func setupViews(){
        bottomContainerView.roundCorner(radius: bottomContainerView.bounds.height / 2)
        getCurrentLocationView.roundCorner(radius: getCurrentLocationView.bounds.height / 2)

        let listTapGesture = UITapGestureRecognizer(target: self, action: #selector(listDidTapped))
        let infoTapGesture = UITapGestureRecognizer(target: self, action: #selector(infoDidTapped))
        let getCurrentLocationTapGesture = UITapGestureRecognizer(target: self, action: #selector(getCurrentLocationDidTapped))
        
        listTapAreaView.addGestureRecognizer(listTapGesture)
        informationTapAreaView.addGestureRecognizer(infoTapGesture)
        getCurrentLocationView.addGestureRecognizer(getCurrentLocationTapGesture)
        
        
        
    }
    
    private func setupMapView(){
        
        // MARK: Marker for current location
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2D(latitude: currentLat, longitude: currentLng)
        sourceMarker.map = self.mapContainerView
        
        
        // MARK: Marker for campus location
        let destinationMarker = GMSMarker()
        destinationMarker.position = CLLocationCoordinate2D(latitude: campusLat, longitude: campusLng)
        destinationMarker.icon = GMSMarker.markerImage(with: Color.shared.greenPrimary)
        destinationMarker.map = self.mapContainerView
        
        let camera = GMSCameraPosition(target: sourceMarker.position, zoom: 15)
        self.mapContainerView.animate(to: camera)
        self.getListBoardingHouse()
    }
    
    private func getListBoardingHouse(){
        
        guard let param = self.searchParam else {return}
        
        DispatchQueue.global().async {
            self.controller.getBoardingHouseList(param: param, completion: { success, boardingHouseList in
                if success {
                    guard let data = boardingHouseList else { return }
                    
                    DispatchQueue.main.async {
                        self.searchResultData = data
                    }
                    
                } else {
                    self.showAlert(message: "Failed to get boarding house data")
                }
            })
        }
    }
    
    private func retrieveMapView(){
        
        let data = self.searchResultData
        let total = data.count
        
        
        for i in 0...total  - 1{
        
            let destinationLat = data[i].latitude
            let destinationLng = data[i].longitude
            
            // MARK: Marker for destination location
            let destinationMarker = GMSMarker()
            destinationMarker.position = CLLocationCoordinate2D(latitude: destinationLat, longitude: destinationLng)
            destinationMarker.icon = GMSMarker.markerImage(with: Color.shared.yellow)
            destinationMarker.map = self.mapContainerView
        }
        
    }

    @objc
    private func listDidTapped(){
        showAlert(message: "Show List!")
    }
    
    @objc
    private func infoDidTapped(){
        let mapInformationViewController = UIStoryboard(name: "Map", bundle: nil).instantiateViewController(withIdentifier: "MapInformationVC")  as! MapInformationViewController
        self.present(mapInformationViewController, animated: true)
    }
    
    @objc
    private func getCurrentLocationDidTapped(){
        
        let sourceMarker = GMSMarker()
        sourceMarker.position = CLLocationCoordinate2D(latitude: currentLat, longitude: currentLng)
        
        let camera = GMSCameraPosition(target: sourceMarker.position, zoom: 15)
        self.mapContainerView.animate(to: camera)
    }
}
