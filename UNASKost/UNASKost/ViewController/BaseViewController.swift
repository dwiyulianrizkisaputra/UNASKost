//
//  BaseViewController.swift
//  UNASKost
//
//  Created by MacBook on 17/06/21.
//

import UIKit

class BaseViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = Color.shared.dark
    }
    
    private func createBackButton(imageName: String = "icBack") -> UIBarButtonItem {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: imageName), for: .normal)
        backButton.addTarget(self, action: #selector(onTapBack(_:)), for: .touchUpInside)
        backButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)

        return UIBarButtonItem(customView: backButton)
    }
    
    private func createBackButton(
        imageName: String = "icBack",
        action: Selector
    ) -> UIBarButtonItem {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: imageName), for: .normal)
        backButton.addTarget(self, action: action, for: .touchUpInside)
        backButton.contentEdgeInsets =
            UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)

        return UIBarButtonItem(customView: backButton)
    }
    
    @objc func onTapBack(_ sender: Any) {
        let isRootViewController = navigationController?.viewControllers.count == 1
        if isRootViewController {
            navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        
        navigationController?.popViewController(animated: true)
    }
}

extension BaseViewController {
    func showBackButton() {
        navigationItem.leftBarButtonItems = [createBackButton()]
    }
    
    func showBackButton(action: Selector) {
        navigationItem.leftBarButtonItems = [createBackButton(action: action)]
    }
    
    func showCustomBackButton(imageName: String, action: Selector) {
        navigationItem.leftBarButtonItems = [createBackButton(imageName: imageName, action: action)]
    }
    
    func showTitle(text: String) {
        let titleLabel = UIButton()
        titleLabel.setTitle(text, for: .normal)
        titleLabel.titleLabel?.textColor = .white
        titleLabel.titleLabel?.font = UIFont.preferredFont(forTextStyle: .title2)
        
        navigationItem.titleView = titleLabel
    }
    
    func showNavigationWithBackButtonAndTitle(title: String) {
        showBackButton()
        showTitle(text: title)
    }
    
    func removeNavigationBarShadow() {
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func showAlert(message: String) {
        let alertViewController = UIAlertController(
            title: "Information",
            message: message,
            preferredStyle: .alert)
        
        alertViewController.addAction(
            UIAlertAction(title: "OK", style: .destructive, handler: { (action) in
                alertViewController.dismiss(animated: true, completion: nil)
        }))
        
        present(alertViewController, animated: true, completion: nil)
    }
}

