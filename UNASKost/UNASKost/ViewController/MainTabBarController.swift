//
//  MainTabBarController.swift
//  UNASKost
//
//  Created by MacBook on 16/06/21.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewControllers = [
            homeViewController(), mapViewController(), reportViewController()
        ]
        
        
        tabBar.isTranslucent = false
        tabBar.backgroundColor = .white

        tabBar.tintColor = Color.shared.greenPrimary
        tabBar.unselectedItemTintColor = .gray

        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()

        tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBar.layer.shadowRadius = 2
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.3
        
        

    }
    
    
    private func homeViewController() -> UIViewController {
        let homeViewController = UIStoryboard(name: "Home", bundle: nil)
            .instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
        
        homeViewController.tabBarItem.title = "Home"
        homeViewController.tabBarItem.image = UIImage(named: "icHome")
        
        let navigationController =
            BaseNavigationViewController(rootViewController: homeViewController)
        return navigationController
    }
    
    private func mapViewController() -> UIViewController {
        let mapViewController = UIStoryboard(name: "Map", bundle: nil)
            .instantiateViewController(withIdentifier: "MapVC") as! MapViewController
        
        mapViewController.tabBarItem.title = "Map"
        mapViewController.tabBarItem.image = UIImage(named: "icMap")
        
        let navigationController =
            BaseNavigationViewController(rootViewController: mapViewController)
        return navigationController
    }
    
    private func reportViewController() -> UIViewController {
        let reportViewController = UIStoryboard(name: "Report", bundle: nil)
            .instantiateViewController(withIdentifier: "ReportVC") as! ReportViewController
        
        reportViewController.tabBarItem.title = "Report"
        reportViewController.tabBarItem.image = UIImage(named: "icList")
        
        let navigationController =
            BaseNavigationViewController(rootViewController: reportViewController)
        return navigationController
    }
}

