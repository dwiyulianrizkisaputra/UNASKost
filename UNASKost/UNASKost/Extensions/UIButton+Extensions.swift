//
//  UIButton+Extensions.swift
//  UNASKost
//
//  Created by MacBook on 19/06/21.
//

import UIKit

extension UIButton{
    
    func setButtonIcon(image: UIImage) {
        self.setImage(image, for: .normal)
        self.imageView?.contentMode = .scaleAspectFit
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
    }
    
}
