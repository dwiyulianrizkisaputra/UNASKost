//
//  UIView+Extensions.swift
//  UNASKost
//
//  Created by MacBook on 16/06/21.
//

import UIKit

extension UIView {
    enum roundCornersType{
        case normal
        case topOnly
        case bottomOnly
        case rightOnly
        case leftOnly
        
        var maskedCorner: CACornerMask {
            switch self {
            case .topOnly:
                return [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            case .bottomOnly:
                return [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            case .rightOnly:
                return [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            case .leftOnly:
                return [.layerMinXMinYCorner, .layerMinXMaxYCorner]
            default:
                return [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
            }
        }
    }
    
    enum ShadowType{
        case top
        case bottom
        case normal
    }

    
    func roundCorner(radius: CGFloat, type: roundCornersType = .normal) {
        layer.cornerRadius = radius
        layer.shadowRadius = radius
        layer.maskedCorners = type.maskedCorner
        clipsToBounds = true
    }
    
    func setShadow(type: ShadowType = .normal){
        switch type {
        case .top:
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOffset = CGSize(width: 0.5, height: -4)
            self.layer.shadowOpacity = 0.25
            self.layer.shadowRadius = 3
        case .bottom:
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: 4)
            self.layer.shadowOpacity = 0.25
            self.layer.shadowRadius = 3
        case .normal:
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOffset = CGSize(width: 0.5, height: 1)
            self.layer.shadowOpacity = 0.25
            self.layer.shadowRadius = 4
        }
    }
}
