//
//  UIImageView+Extensions.swift
//  UNASKost
//
//  Created by MacBook on 20/06/21.
//

import UIKit
import Kingfisher

extension UIImageView{
    func loadImage(url: URL) {
        kf.setImage(with: url, placeholder: UIImage(named: "placeholderImage"))
    }
}
