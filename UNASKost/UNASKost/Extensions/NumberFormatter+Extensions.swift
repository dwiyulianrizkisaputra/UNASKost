//
//  NumberFormatter+Extensions.swift
//  UNASKost
//
//  Created by MacBook on 19/06/21.
//

import Foundation

extension NumberFormatter {
    func toDisplayFormat(value: Double, showCurrencyCode: Bool = false) -> String {
        let indonesiaLocale = Locale.init(identifier: "id_ID")
        let value = Int(value)
        
        numberStyle = .currency
        locale = indonesiaLocale
        maximumFractionDigits = 2
        currencySymbol = showCurrencyCode ? indonesiaLocale.currencySymbol : ""
        
        return string(from: NSNumber(value: value)) ?? ""
    }
}
